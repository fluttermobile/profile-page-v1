import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARk}
void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatefulWidget {
  const ContactProfilePage({super.key});

  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARk
      ? MyAppTheme.appThemeLight()
      : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget,
        body: buildBodyWidget,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARk
              ? currentTheme = APP_THEME.LIGHT
              : currentTheme = APP_THEME.DARk;
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("0611415146"),
    subtitle: Text("extra"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTitle() {
  return ListTile(
    leading: Icon(null),
    title: Text("0611645564"),
    subtitle: Text("Mom"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mailListTitle() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("63160085@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget locationListTitle() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("Thawung Lopburi 15150"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}

var buildAppBarWidget = AppBar(
  // backgroundColor: Color.fromARGB(255, 255, 134, 5),
  leading: Icon(
    Icons.arrow_back,
    // color: Color.fromARGB(255, 255, 255, 255),
  ),
  actions: <Widget>[
    IconButton(
        onPressed: () {
          print("Contact is starred");
        },
        icon: Icon(
          Icons.star_border,
          // color: Color.fromARGB(255, 255, 255, 255),
        ))
  ],
);

var buildBodyWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
//Height constraint at Container widget level
          height: 250,
          child: Image.network(
            "https://static.wikia.nocookie.net/himoto-umaruchan/images/a/a2/Umaru%27s_anime_design_%28chibi%29.png/revision/latest?cb=20200411195915",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Arthit Muangsiri",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              // buildCallButton(),
              // buildTextButton(),
              // buildVideoCallButton(),
              // buildEmailButton(),
              // buildDirectionsButton(),
              // buildPayButton(),
            ],
          ),
        ),
        
        Divider(color: Colors.grey
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Theme(
            data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.pink,
              ),
            ),
            child: profileActionItems(),
          ),
        ),
        Divider(color: Colors.grey),
        mobilePhoneListTile(),
        otherPhoneListTitle(),
        mailListTitle(),
        locationListTitle(),
      ],
    )
  ],
);

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.indigo.shade500,
        ),
      );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.black,
            iconTheme: IconThemeData(
              color: Colors.white
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.pink.shade500,
        ),
      );
  }
}
